import 'package:flutter/material.dart';
import 'validate.dart';
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login Page',
      home: Scaffold(
        appBar: AppBar(title: Text('Login Form & Validation'),),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget>  with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  String errorMessage = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.mail_sharp),
        labelText: 'Email address',
        hintText: 'domain@abc.com',
        errorText: errorMessage,
      ),
      validator: validateEmail,

      onSaved: (value) {
        email = value as String;
      },
      onChanged: (String value) {
        print('onChanged email: $value');
        if (!value.contains('@')) {
          errorMessage = '$value is an invalid email.';

        } else if (!value.contains('.')) {
          errorMessage = '$value is an invalid email.';
        } else {
          errorMessage = '';
        }
        setState(() {
          print('Update the state of the screen...');
        });
      },

    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.password),
        labelText: 'Password',
        hintText: '**********',
      ),
      validator: validatePassword,
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
            print('password=$password');
          }
        },
        child: Text('Submit')
    );
  }
}